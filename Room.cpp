#include "Room.h"
#include <algorithm>

Room::Room(const std::string& description) : description(description) {}

void Room::AddExit(const std::string& direction, Room* room) {
    exits[direction] = room;
}

void Room::AddItem(const Item& item) {
    items.push_back(item);
}

void Room::RemoveItem(const Item& item) {
    items.erase(std::remove_if(items.begin(), items.end(), [&](const Item& i) {
        return i.GetName() == item.GetName();
        }), items.end());
}

const std::string& Room::GetDescription() const {
    return description;
}

Room* Room::GetExit(const std::string& direction) {
    auto it = exits.find(direction);
    if (it != exits.end()) {
        return it->second;
    }
    return nullptr;
}

const std::vector<Item>& Room::GetItems() const {
    return items;
}

const std::map<std::string, Room*>& Room::GetExits() const {
    return exits;
}
