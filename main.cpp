#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include "Item.h"
#include "Room.h"
#include "Character.h"
#include "Player.h"
#include "Area.h"
#include "GameSaverLoader.h"
#include "CommandInterpreter.h"



int main() {
    Area gameWorld;
    GameSaverLoader gsl;
    // Assuming the file path might be updated according to your actual file location
    gameWorld.LoadMapFromFile("game_map.txt");

    Player player("Alice", 100);
    Room* currentRoom = gameWorld.GetRoom("startRoom");
    player.SetLocation(currentRoom);

    CommandInterpreter interpreter(&player);

    // Welcome message
    std::cout << "Welcome to the Text Adventure Game!\n"
        << "Explore the world, interact with objects, and uncover secrets.\n"
        << "Type 'help' for a list of commands.\n" << std::endl;
    std::cout << "Available commands:\n"
        << "  move [direction] - Move in the specified direction (north, south, east, west).\n"
        << "  look - Get a description of your current location.\n"
        << "  pick up [item] - Pick up an item in the current room.\n"
        << "  quit - Exit the game.\n"
        << "  load - Loads the game from its last saved state.\n"
        << std::endl;



    while (true) {
        std::cout << "> ";
        std::string command;
        std::getline(std::cin, command);

        if (command == "quit") {
            std::cout << "Goodbye!" << std::endl;
            break;
        }
        else if (command == "help") {
            std::cout << "Available commands:\n"
                << "  move [direction] - Move in the specified direction (north, south, east, west).\n"
                << "  look - Get a description of your current location.\n"
                << "  pick up [item] - Pick up an item in the current room.\n"
                << "  quit - Exit the game.\n"
                << std::endl;
        }
        else if (command == "save") {
            const std::map<std::string, Room*>& allRooms = gameWorld.GetAllRooms();
            gsl.saveGame(player, allRooms, "savefile.dat");
        }
        else if (command == "load") {
            gsl.loadGame("savefile.dat");
        }
        else {
            interpreter.interpretCommand(command);
            const std::map<std::string, Room*>& allRooms = gameWorld.GetAllRooms();
            gsl.saveGame(player, allRooms, "savefile.dat");
        }

    }

    return 0;
}
