#pragma once
#include <iostream>
#include <sstream>
#include "Player.h" // Include Player header for Player class usage

class CommandInterpreter {
private:
    Player* player_;

public:
    CommandInterpreter(Player* player);
    void interpretCommand(const std::string& command);
};

