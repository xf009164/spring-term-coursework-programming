#pragma once
#include <string>
#include "Character.h" // Include Character header for Character class usage
#include "Room.h" // Include Room header for Room class usage

class Player : public Character {
private:
    Room* location;

public:
    Player(const std::string& name, int health);
    void SetLocation(Room* room);
    Room* GetLocation() const;
    void move(const std::string& direction);
    void lookAround() const;
    void pickUpItem(const std::string& itemName);
};
