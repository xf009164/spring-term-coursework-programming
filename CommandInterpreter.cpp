#include "CommandInterpreter.h"

CommandInterpreter::CommandInterpreter(Player* player) : player_(player) {}

void CommandInterpreter::interpretCommand(const std::string& command) {
    std::istringstream iss(command);
    std::string action;
    iss >> action;

    if (action == "move") {
        std::string direction;
        if (iss >> direction) {
            player_->move(direction);
        }
        else {
            std::cout << "Move where?" << std::endl;
        }
    }
    else if (action == "look") {
        player_->lookAround();
    }
    else if (action == "pick") {
        std::string up, itemName;
        iss >> up; // Read and discard the word "up"
        std::getline(iss, itemName); // Read the rest of the line as the item name
        itemName.erase(0, itemName.find_first_not_of(' ')); // Trim leading whitespace
        if (!itemName.empty()) {
            player_->pickUpItem(itemName);
        }
        else {
            std::cout << "Pick up what?" << std::endl;
        }
    }
    else {
        std::cout << "Unknown command: " << action << std::endl;
    }
}
