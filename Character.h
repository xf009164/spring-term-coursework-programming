#pragma once
#include <string>
#include <vector>
#include <iostream>
#include "Item.h" // Include Item header for Item class usage

class Character {
protected:
    std::string name;
    int health;
    std::vector<Item> inventory;

public:
    Character(const std::string& name, int health);
    void TakeDamage(int damage);
    int GetHealth() const;
    void SetHealth(int newHealth);
    const std::vector<Item>& GetInventory() const;
    void ClearInventory();
    void AddItem(const Item& item);
};

