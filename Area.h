#pragma once
#include <map>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include "Room.h" // Include Room header for Room class usage
#include "Item.h" // Include Item header for Item class usage

class Area {
private:
    std::map<std::string, Room*> rooms;

public:
    void AddRoom(const std::string& name, Room* room);
    Room* GetRoom(const std::string& name);
    void ConnectRooms(const std::string& room1Name, const std::string& room2Name, const std::string& direction);
    std::string GetOppositeDirection(const std::string& direction);
    const std::map<std::string, Room*>& GetAllRooms() const;
    void LoadMapFromFile(const std::string& filename);
};

