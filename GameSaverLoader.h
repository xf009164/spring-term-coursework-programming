#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include "Player.h"
#include "Room.h" // Include Room header for Room class usage
#include "Item.h" // Include Item header for Item class usage

class GameSaverLoader {
public:
    void saveGame(const Player& player, const std::map<std::string, Room*>& allRooms, const std::string& filename);
    void loadGame(const std::string& filename);
};

