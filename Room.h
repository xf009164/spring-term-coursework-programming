#pragma once
#include <string>
#include <map>
#include <vector>
#include "Item.h" // Include Item header for Item class usage

class Room {
private:
    std::string description;
    std::map<std::string, Room*> exits;
    std::vector<Item> items;

public:
    Room(const std::string& description);
    void AddExit(const std::string& direction, Room* room);
    void AddItem(const Item& item);
    void RemoveItem(const Item& item);
    const std::string& GetDescription() const;
    Room* GetExit(const std::string& direction);
    const std::vector<Item>& GetItems() const;
    const std::map<std::string, Room*>& GetExits() const;
};

