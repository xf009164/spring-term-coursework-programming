#pragma once
#include <string>
#include <iostream>

class Item {
private:
    std::string name;
    std::string description;

public:
    Item(const std::string& name, const std::string& description);
    void Interact() const;
    const std::string& GetName() const;
    const std::string& GetDescription() const;
};

