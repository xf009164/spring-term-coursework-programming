#include "Character.h"

Character::Character(const std::string& name, int health) : name(name), health(health) {}

void Character::TakeDamage(int damage) {
    health -= damage;
    std::cout << name << " takes " << damage << " damage." << std::endl;
}

int Character::GetHealth() const {
    return health;
}

void Character::SetHealth(int newHealth) {
    health = newHealth;
}

const std::vector<Item>& Character::GetInventory() const {
    return inventory;
}

void Character::ClearInventory() {
    inventory.clear();
}

void Character::AddItem(const Item& item) {
    inventory.push_back(item);
}
