#include "Item.h"

Item::Item(const std::string& name, const std::string& description)
    : name(name), description(description) {}

void Item::Interact() const {
    std::cout << "Interacting with " << name << ": " << description << std::endl;
}

const std::string& Item::GetName() const {
    return name;
}

const std::string& Item::GetDescription() const {
    return description;
}
