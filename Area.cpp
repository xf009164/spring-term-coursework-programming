#include "Area.h"

void Area::AddRoom(const std::string& name, Room* room) {
    rooms[name] = room;
}

Room* Area::GetRoom(const std::string& name) {
    auto it = rooms.find(name);
    if (it != rooms.end()) {
        return it->second;
    }
    return nullptr;
}

void Area::ConnectRooms(const std::string& room1Name, const std::string& room2Name, const std::string& direction) {
    Room* room1 = GetRoom(room1Name);
    Room* room2 = GetRoom(room2Name);
    if (room1 && room2) {
        room1->AddExit(direction, room2);
        // Add the opposite direction automatically (for simplicity)
        std::string oppositeDirection = GetOppositeDirection(direction);
        if (!oppositeDirection.empty()) {
            room2->AddExit(oppositeDirection, room1);
        }
    }
}

std::string Area::GetOppositeDirection(const std::string& direction) {
    // Simple mapping, can be expanded
    std::map<std::string, std::string> opposites = {
        {"north", "south"},
        {"south", "north"},
        {"east", "west"},
        {"west", "east"}
    };
    auto it = opposites.find(direction);
    if (it != opposites.end()) {
        return it->second;
    }
    return "";
}

const std::map<std::string, Room*>& Area::GetAllRooms() const {
    return rooms;
}

void Area::LoadMapFromFile(const std::string& filename) {
    std::ifstream file(filename);
    if (!file.is_open()) {
        std::cerr << "Failed to open the file: " << filename << std::endl;
        return;
    }

    std::string line;
    while (std::getline(file, line)) {
        std::istringstream iss(line);
        std::string room1Name, room2Name, direction, itemsSection;
        if (std::getline(iss, room1Name, '|') &&
            std::getline(iss, room2Name, '|') &&
            std::getline(iss, direction, '|') &&
            std::getline(iss, itemsSection)) {

            // Create rooms if they don't exist
            if (rooms.find(room1Name) == rooms.end()) {
                rooms[room1Name] = new Room(room1Name);
            }
            if (rooms.find(room2Name) == rooms.end()) {
                rooms[room2Name] = new Room(room2Name);
            }

            // Connect the rooms
            ConnectRooms(room1Name, room2Name, direction);

            // Now process the itemsSection for room1Name
            std::istringstream itemsStream(itemsSection);
            std::string itemPair;
            while (std::getline(itemsStream, itemPair, ';')) { // Using ';' to separate items
                size_t colonPos = itemPair.find(':');
                if (colonPos != std::string::npos) {
                    std::string itemName = itemPair.substr(0, colonPos);
                    std::string itemDesc = itemPair.substr(colonPos + 1);
                    rooms[room1Name]->AddItem(Item(itemName, itemDesc));
                }
            }
        }
    }
}
