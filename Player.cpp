#include "Player.h"
#include <iostream>
#include <algorithm>

Player::Player(const std::string& name, int health) : Character(name, health), location(nullptr) {}

void Player::SetLocation(Room* room) {
    location = room;
}

Room* Player::GetLocation() const {
    return location;
}

void Player::move(const std::string& direction) {
    if (location) {
        Room* nextRoom = location->GetExit(direction);
        if (nextRoom) {
            SetLocation(nextRoom);
            std::cout << "You move " << direction << " to " << location->GetDescription() << std::endl;
            lookAround(); // Optionally, automatically look around after moving
        }
        else {
            std::cout << "You can't go that way." << std::endl;
        }
    }
    else {
        std::cout << "You are nowhere. Something went wrong." << std::endl;
    }
}

void Player::lookAround() const {
    if (location) {
        std::cout << "You are at: " << location->GetDescription() << std::endl;
        std::cout << "You see exits to the: ";
        for (const auto& exit : location->GetExits()) {
            std::cout << exit.first << " ";
        }
        std::cout << std::endl;
        if (!location->GetItems().empty()) {
            std::cout << "Items in the room: ";
            for (const auto& item : location->GetItems()) {
                std::cout << item.GetName() << " ";
            }
            std::cout << std::endl;
        }
        else {
            std::cout << "There are no items in this room." << std::endl;
        }
    }
    else {
        std::cout << "It's pitch black. You can't see anything." << std::endl;
    }
}

void Player::pickUpItem(const std::string& itemName) {
    if (!location) {
        std::cout << "You are nowhere. Unable to pick up anything." << std::endl;
        return;
    }

    auto& items = location->GetItems();
    auto it = std::find_if(items.begin(), items.end(), [&itemName](const Item& item) {
        return item.GetName() == itemName;
        });

    if (it != items.end()) {
        inventory.push_back(*it); // Assumes inventory is a vector of Items
        location->RemoveItem(*it);
        std::cout << "Picked up " << itemName << "." << std::endl;
    }
    else {
        std::cout << "Item not found: " << itemName << std::endl;
    }
}
