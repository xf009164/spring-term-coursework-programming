#include "GameSaverLoader.h"

void GameSaverLoader::saveGame(const Player& player, const std::map<std::string, Room*>& allRooms, const std::string& filename) {
    // Aggregate room descriptions
    std::string roomDescriptions = "Rooms:";
    // Aggregate items across all rooms
    std::string allItems = "Items:";

    for (const auto& pair : allRooms) {
        const Room* room = pair.second; // Corrected to pointer
        // Add the room description to the aggregated string
        if (roomDescriptions.size() > 6) roomDescriptions += "; ";
        roomDescriptions += room->GetDescription(); // Corrected to pointer access

        // Add items from the room to the aggregated items string
        for (const auto& item : room->GetItems()) { // Corrected to pointer access
            if (allItems.size() > 6) allItems += "; ";
            allItems += item.GetName() + ":" + item.GetDescription();
        }
    }

    // Open the file to write the aggregated states
    std::ofstream fileOut(filename);
    if (!fileOut.is_open()) {
        std::cerr << "Failed to open file for saving: " << filename << std::endl;
        return;
    }

    // Write room descriptions on the first line
    fileOut << roomDescriptions << '\n';
    // Write all items on the second line
    fileOut << allItems << '\n';

    // Optionally save player-specific data, like health and inventory, in subsequent lines
    fileOut << "Player Health: " << player.GetHealth() << '\n';
    // Assuming player's inventory might also need to be saved
    std::string playerItems = "Player Items:";
    for (const auto& item : player.GetInventory()) {
        if (playerItems.size() > 12) playerItems += "; ";
        playerItems += item.GetName() + ":" + item.GetDescription();
    }
    fileOut << playerItems << '\n';

    fileOut.close();
    std::cout << "Game saved to " << filename << std::endl;
}

void GameSaverLoader::loadGame(const std::string& filename) {
    std::ifstream fileIn(filename);
    std::string line;

    if (!fileIn.is_open()) {
        std::cerr << "Failed to open file for loading: " << filename << std::endl;
        return;
    }

    while (std::getline(fileIn, line)) {
        // Simply print each line to the console
        std::cout << line << std::endl;
    }

    fileIn.close();
    std::cout << "Game data loaded and displayed from " << filename << std::endl;
}
